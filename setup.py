from setuptools import setup
from os import path
from io import open
import sys

here = path.abspath(path.dirname(__file__))

version_maj = sys.version_info[0]
version_min = sys.version_info[1]

if version_maj < 3:
    print("Need python 3 to run. You are trying to install this with python {}.{}".format(str(version_maj), str(version_min)))
    sys.exit(1)

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name="backuptool",
    version="0.5",
    description="A backup and recovery tool.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://placeholder.com",  # Optional
    author="Amartya Datta Gupta",
    author_email="amartya00@gmail.com",  # Optional
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: Developers',
        'Topic :: File management :: Backup and restore',
        'License :: GPLV2',
        'Programming Language :: Python :: 3.7',
    ],
    packages=["backuptool", "backuptool.activities", "backuptool.deciders", "backuptool.models"],
    install_requires=["boto3", "botocore", "pytz", "urllib3"],
    extras_require={
        'dev': [],
        'test': ["coverage"],
    },
    entry_points={
        "console_scripts": [
            "backuptool=backuptool:main",
        ],
    },
    project_urls={
        "Source": "https://placeholder.com"
    },
)