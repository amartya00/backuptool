import sys
import os

sys.dont_write_bytecode = True
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + "/../")

from backuptool.deciders.runner import execute


def main():
    execute()