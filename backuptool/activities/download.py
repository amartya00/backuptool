import boto3
import os

import urllib3
from botocore.exceptions import ClientError, ReadTimeoutError
from backuptool.models.log import Log
from backuptool.models.retries import RetryableException, DoWithExponentialRetry


class Download (DoWithExponentialRetry):
    def __init__(self, logger: Log, config: dict):
        self.log = logger
        self.s3_prefix = config["S3Prefix"]
        self.s3_bucket = config["S3Bucket"]
        self.client = boto3.client("s3")
        super(Download, self).__init__(3600)

    def action(self, **kwargs):
        filename = kwargs["filename"]
        full_path = kwargs["full_path"]
        try:
            save_folder = os.path.dirname(full_path)
            if not os.path.isdir(save_folder):
                self.log.info("Creating folder {} .".format(save_folder))
                os.makedirs(save_folder)
            with open(full_path, "wb+") as fp:
                key = filename if self.s3_prefix == "" else self.s3_prefix + "/" + filename
                response = self.client.get_object(
                    Bucket=self.s3_bucket,
                    Key=key
                )
                # TODO: Split the read() into smaller chunk reads
                fp.write(response["Body"].read())
                return
        except ClientError as e:
            self.log.warn("Received exception {} while trying to download.".format(e.response["Error"]["Code"]))
            raise e
        except urllib3.exceptions.ReadTimeoutError as e1:
            self.log.warn("Received exception {} while trying to download.".format(str(e1)))
            raise RetryableException("Received exception {} while trying to download.".format(str(e1)))
        except ReadTimeoutError as e2:
            self.log.warn("Received exception {} while trying to download.".format(str(e2)))
            raise RetryableException("Received exception {} while trying to download.".format(str(e2)))

    def run(self, files: "list[(str, str, float)]"):
        num_files = len(files)
        count = 1
        for full_path, file_name, file_size in files:
            self.log.info("Attempting to save file {} [{} out of {}].".format(full_path, str(count), str(num_files)))
            self.do_with_retry(filename=file_name, full_path=full_path)
            count = count + 1
