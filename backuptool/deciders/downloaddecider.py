from backuptool.models.log import Log
from backuptool.activities.gatherdownloadfiles import GatherDownloadFilesActivity
from backuptool.activities.download import Download


class DownloadDecider:
    def __init__(self, logger: Log, config: dict):
        self.log = logger
        self.gather_files_activity = GatherDownloadFilesActivity(logger, config)
        self.download = Download(logger, config)

    def run(self):
        try:
            self.log.info("Gathering files to download.")
            files = self.gather_files_activity.run()
            self.log.info("Downloading files.")
            self.download.run(files)
            self.log.info("Done!!")
        except ValueError as e:
            self.log.error("Failed to complete download operation because:\n'{}'.\nTry again later".format(e))

