import boto3
import urllib3
from botocore.exceptions import ClientError, ReadTimeoutError
from backuptool.models.log import Log
from backuptool.models.retries import RetryableException, DoWithExponentialRetry


class Upload (DoWithExponentialRetry):
    def __init__(self, logger: Log, config: dict):
        self.log = logger
        self.s3_prefix = config["S3Prefix"]
        self.s3_bucket = config["S3Bucket"]
        self.client = boto3.client("s3")
        super(Upload, self).__init__(self, 3600)

    def action(self, **kwargs):
        filename = kwargs["filename"]
        full_path = kwargs["full_path"]

        try:
            with open(full_path, "rb") as fp:
                self.log.info("Attempting to upload file {}.".format(filename))
                key = filename if self.s3_prefix == "" else self.s3_prefix + "/" + filename
                self.client.put_object(
                    ACL="private",
                    Body=fp,
                    Bucket=self.s3_bucket,
                    Key=key
                )
                return
        except ClientError as e:
            self.log.warn("Received exception {} while trying to upload.".format(e.response["Error"]["Code"]))
            raise e
        except urllib3.exceptions.ReadTimeoutError as e1:
            self.log.warn("Received exception {} while trying to download.".format(str(e1)))
            raise RetryableException("Received exception {} while trying to download.".format(str(e1)))
        except ReadTimeoutError as e2:
            self.log.warn("Received exception {} while trying to download.".format(str(e2)))
            raise RetryableException("Received exception {} while trying to download.".format(str(e2)))

    def run(self, files: "list[(str, str, float)]"):
        for full_path, file_name, file_size in files:
            self.do_with_retry(filename=file_name, full_path=full_path)
