import os
import boto3
import datetime
import pytz
import sys

import urllib3
from botocore.exceptions import ClientError, NoCredentialsError
from backuptool.models.log import Log


class GatherFilesActivity:
    def __init__(self, logger: Log, config: "dict"):
        self.root = config["Root"]
        self.dirs = config["Dirs"]
        self.blacklist = GatherFilesActivity.process_blacklist(config["Blacklist"]) if "Blacklist" in config else set()
        self.ignore_ext = set([e.replace(".", "") for e in (GatherFilesActivity.process_blacklist(config["IgnoreFileExt"]) if "IgnoreFileExt" in config else set())])
        self.s3_prefix = config["S3Prefix"]
        self.s3_bucket = config["S3Bucket"]
        self.log = logger
        self.s3 = boto3.client("s3")
        self.log.info("Blacklist: {}".format(str(self.blacklist)))

    @staticmethod
    def process_blacklist(l: "list[str]"):
        retval = set()
        [retval.add(elem[:-1] if elem.endswith("/") else elem) for elem in l]
        return retval

    def recursively_gather_files(self, basedir: "str"):
        self.log.info("Examining folder {}".format(basedir))

        # Check if its blacklisted
        normalized_full_path = basedir[:-1] if basedir.endswith("/") else basedir
        if normalized_full_path in self.blacklist:
            self.log.info("Skipping folder {} as it is blacklisted.".format(normalized_full_path))
            return {}

        files = {}
        # Sometimes the given base directory might not exist on a machine.
        if not os.path.isdir(basedir):
            return files
        for f in os.listdir(basedir):
            full_path = os.path.join(basedir, f)
            if os.path.islink(full_path):
                continue
            elif os.path.isdir(full_path):
                gathered_files = self.recursively_gather_files(full_path)
                for k in gathered_files:
                    files[k] = gathered_files[k]
            elif full_path.split(".")[-1] not in self.ignore_ext:
                file_size = os.path.getsize(full_path)
                lm = pytz.UTC.localize(datetime.datetime.utcfromtimestamp(os.path.getmtime(full_path)))
                files[full_path] = (file_size, lm)
                self.log.info("Examining file {} of size {} MB.".format(
                    full_path, str(file_size/(1024*1024))
                ))
            else:
                ext = full_path.split(".")[-1]
                self.log.info("Ignoring file {} as the extension ({}) is blacklisted.".format(full_path, ext))
        return files

    def process_file_names(self, files: "dict[str, (int, datetime)]"):
        processed_files = {}
        for f in files:
            s, lm = files[f]
            tmp_filename = f.replace(self.root, "")
            if tmp_filename.startswith("/"):
                tmp_filename = tmp_filename[1:]
            processed_files[tmp_filename] = (f, s, lm)
        return processed_files

    def get_uploaded_files(self):
        files = {}
        self.log.info("Making calls to S3 to list uploaded files.")
        try:
            response = self.s3.list_objects(
                Bucket=self.s3_bucket,
                Prefix=self.s3_prefix
            )
            for k, lm, sz in [(r["Key"], r["LastModified"], r["Size"]) for r in response["Contents"]]:
                k = k.replace(self.s3_prefix, "")
                if k.startswith("/"):
                    k = k[1:]
                self.log.info("Received info on file {}.".format(k))
                if k.split(".")[-1] not in self.ignore_ext:
                    files[k] = (lm, sz)
                else:
                    self.log.info("Ignoring file {} for download because its extension is blacklisted.".format(k.split(".")[-1]))
            is_truncated = response["IsTruncated"]
            next_token = response["Contents"][-1]["Key"]
        except ClientError as e:
            self.log.error("Could not read S3 bucket because: {}.".format(e.response["Error"]["Message"]))
            sys.exit(1)
        except NoCredentialsError as e:
            self.log.error("AWS credentials are not configured. Please install aws cli and run `aws configure`.")
            sys.exit(1)
        except urllib3.exceptions.ReadTimeoutError as e1:
            self.log.warn("Received exception {} while trying to fetch information.".format(str(e1)))
            sys.exit(1)

        while is_truncated:
            try:
                self.log.info("Making calls to S3 to list uploaded files.")
                response = self.s3.list_objects(
                    Bucket=self.s3_bucket,
                    Prefix=self.s3_prefix,
                    Marker=next_token
                )
                for k, lm, sz in [(r["Key"], r["LastModified"], r["Size"]) for r in response["Contents"]]:
                    k = k.replace(self.s3_prefix, "")
                    if k.startswith("/"):
                        k = k[1:]
                    self.log.info("Received info on file {}.".format(k))
                    if k.split(".")[-1] not in self.ignore_ext:
                        files[k] = (lm, sz)
                    else:
                        self.log.info("Ignoring file {} for download because its extension is blacklisted.".format(k.split(".")[-1]))
                is_truncated = response["IsTruncated"]
                next_token = response["Contents"][-1]["Key"]
            except ClientError as e:
                self.log.error("Could not read S3 bucket because: {}.".format(e.response["Error"]["Message"]))
                sys.exit(1)
            except NoCredentialsError as e:
                self.log.error("AWS credentials are not configured. Please install aws cli and run `aws configure`.")
                sys.exit(1)
            except urllib3.exceptions.ReadTimeoutError as e1:
                self.log.warn("Received exception {} while trying to fetch information.".format(str(e1)))
                sys.exit(1)
        return files


