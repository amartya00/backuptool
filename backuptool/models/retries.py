import time


class RetryableException(Exception):
    pass


class DoWithExponentialRetry:
    def __init__(self, max_timeout_seconds: "int" = 3600):
        self.max_timeout_seconds = max_timeout_seconds

    def action(self, **kwargs):
        pass

    def do_with_retry(self, **kwargs):
        start_time = time.time()
        step_time = time.time()
        while step_time - start_time < self.max_timeout_seconds:
            step_time = time.time()
            try:
                return self.action(**kwargs)
            except RetryableException as e:
                time.sleep(2.717 ** (step_time - start_time))
        raise ValueError("Failed to complete action because {}".format(e))
