import os
import json
import copy


class Config:
    CONFIG_DIR = os.path.join(os.environ["HOME"], ".backuptool")
    CONFIG_FILE = os.path.join(CONFIG_DIR, "config.json")
    DEFAULT_CONFIG = {
        "Root": os.environ["HOME"],
        "Level": "INFO",
        "Dirs": [
            "Documents",
            "Downloads",
            "Pictures",
            "Music",
            "Videos"
        ],
        "S3Bucket": "BackupTool",
        "S3Prefix":  "BackupTool",
        "ProgramName": "BackupTool",
        "LogFile": os.path.join(CONFIG_DIR, "log.log")
    }

    @staticmethod
    def first_run():
        if not os.path.isdir(Config.CONFIG_DIR):
            os.makedirs(Config.CONFIG_DIR)
        if not os.path.isfile(Config.CONFIG_FILE):
            with open(Config.CONFIG_FILE, "w") as fp:
                fp.write(json.dumps(Config.DEFAULT_CONFIG, indent=4))

    @staticmethod
    def interactive_first_run():
        print("Doing an interactive setup.\n")
        bucket_name = input("Please enter your AWS bucket that you want to use for backing up [default = {}]: "
                            .format(Config.DEFAULT_CONFIG["S3Bucket"]))
        bucket_name = Config.DEFAULT_CONFIG["S3Bucket"] if bucket_name == "" else bucket_name

        prefix = input("Please provide a prefix that will be used inside the bucket for saving data [default = {}]: "
                       .format(Config.DEFAULT_CONFIG["S3Prefix"]))
        prefix = Config.DEFAULT_CONFIG["S3Prefix"] if prefix == "" else prefix

        root = input("Please provide root for backup [default = {}]: ".format(Config.DEFAULT_CONFIG["Root"]))
        root = Config.DEFAULT_CONFIG["Root"] if root == "" else root

        conf = copy.deepcopy(Config.DEFAULT_CONFIG)
        conf["S3Bucket"] = bucket_name
        conf["S3Prefix"] = prefix
        conf["Root"] = root
        if not os.path.isdir(Config.CONFIG_DIR):
            os.makedirs(Config.CONFIG_DIR)
        with open(Config.CONFIG_FILE, "w") as fp:
            fp.write(json.dumps(conf, indent=4))

    @staticmethod
    def current_config():
        if not os.path.isfile(Config.CONFIG_FILE):
            return None
        else:
            with open(Config.CONFIG_FILE, "r") as fp:
                return json.loads(fp.read())

    def __init__(self):
        if not os.path.isfile(Config.CONFIG_FILE):
            Config.interactive_first_run()

        # Load config
        with open(Config.CONFIG_FILE, "r") as fp:
            self.config = json.loads(fp.read())
