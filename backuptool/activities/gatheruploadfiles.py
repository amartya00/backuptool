import os
import datetime

from backuptool.activities.gatherfiles import GatherFilesActivity
from backuptool.models.log import Log


class GatherUploadFilesActivity (GatherFilesActivity):
    def __init__(self, logger: Log, config: "dict"):
        super(GatherUploadFilesActivity, self).__init__(logger, config)

    def compare_files(self, uploaded_files: "dict[str, (datetime, int)]", local_files: "dict[str, (str, int, datetime)]"):
        final_file_list = []
        for key in local_files:
            if key in uploaded_files:
                full, sz, lm_l = local_files[key]
                lm_u, sz_u = uploaded_files[key]
                if lm_l > lm_u:
                    self.log.info("File {} needs to be backed up as it was modified after last backup.".format(key))
                    final_file_list.append((full, key, sz))
                else:
                    self.log.info("File {} has been properly backed up.".format(key))
            else:
                full, sz, lm_l = local_files[key]
                self.log.info("File {} needs to be backed up as it has not been yet.".format(key))
                final_file_list.append((full, key, sz))
        return final_file_list

    def run(self):
        self.log.info("Gathering a list of all local files.")
        # Gather all uploaded files
        uploaded_files = self.get_uploaded_files()

        # Gather all file paths
        unprocessed_files = {}
        for d in self.dirs:
            files = self.recursively_gather_files(os.path.join(self.root, d))
            for f in files:
                unprocessed_files[f] = files[f]

        # Process them
        processed_files = self.process_file_names(unprocessed_files)

        # Filter them
        final_file_list = self.compare_files(uploaded_files, processed_files)

        total_files = len(final_file_list)
        total_size = sum([elem[2] for elem in final_file_list]) / (1024*1024*1024)
        self.log.info("Gathered {} files of total size {} GB.".format(str(total_files), str(total_size)))
        return final_file_list
