import os
import datetime

from backuptool.activities.gatherfiles import GatherFilesActivity
from backuptool.models.log import Log


class GatherDownloadFilesActivity (GatherFilesActivity):
    def __init__(self, logger: Log, config: "dict"):
        super(GatherDownloadFilesActivity, self).__init__(logger, config)

    def compare_files(self, uploaded_files: "dict[str, (datetime, int)]", local_files: "dict[str, (str, int, datetime)]"):
        final_file_list = []
        for key in uploaded_files:
            if key in local_files:
                full, sz, lm_l = local_files[key]
                lm_u, sz_u = uploaded_files[key]
                if lm_l < lm_u:
                    self.log.info("File {} needs to be downloaded as it was modified after last download.".format(key))
                    final_file_list.append((full, key, sz_u))
                else:
                    self.log.info("File {} has been properly restored.".format(key))
            else:
                # Since the file does not exist locally, we have to construct the filename like:
                # full path = backup root + s3 key (without prefix)
                key_name = key.replace(self.s3_prefix, "")
                normalized_key = key_name[1:] if key_name.startswith("/") else key_name
                full = os.path.join(self.root, normalized_key)
                lm_u, sz_u = uploaded_files[key]
                self.log.info("File {} needs to be downloaded up as it has not restored yet.".format(full))
                final_file_list.append((full, key, sz_u))
        return final_file_list

    def run(self):
        self.log.info("Gathering a list of all local files.")
        # Gather all uploaded files
        uploaded_files = self.get_uploaded_files()

        # Gather all local file paths
        unprocessed_files = {}
        for d in self.dirs:
            files = self.recursively_gather_files(os.path.join(self.root, d))
            for f in files:
                unprocessed_files[f] = files[f]

        # Process them
        processed_files = self.process_file_names(unprocessed_files)

        # Filter them
        final_file_list = self.compare_files(uploaded_files, processed_files)

        total_files = len(final_file_list)
        total_size = sum([elem[2] for elem in final_file_list]) / (1024*1024*1024)
        self.log.info("Gathered {} files of total size {} GB to be downloaded.".format(str(total_files), str(total_size)))
        return final_file_list
