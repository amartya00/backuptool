from backuptool.models.log import Log
from backuptool.activities.gatheruploadfiles import GatherUploadFilesActivity
from backuptool.activities.upload import Upload


class UploadDecider:
    def __init__(self, logger: Log, config: dict):
        self.log = logger
        self.gather_files_activity = GatherUploadFilesActivity(logger, config)
        self.upload_activity = Upload(logger, config)

    def run(self):
        try:
            self.log.info("Gathering files to upload.")
            files = self.gather_files_activity.run()
            self.log.info("Uploading files.")
            self.upload_activity.run(files)
            self.log.info("Done!!")
        except ValueError as e:
            self.log.error("Failed to complete download operation because:\n'{}'.\nTry again later".format(e))

