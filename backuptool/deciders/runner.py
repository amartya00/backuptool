import argparse
import sys

from backuptool.deciders.uploaddecider import UploadDecider
from backuptool.deciders.downloaddecider import DownloadDecider
from backuptool.models.config import Config
from backuptool.models.log import Log


def execute():
    parser = argparse.ArgumentParser(prog="backuptool", description=__doc__, usage="backuptool [options]",
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-c", "--current", help="Show current setup.", action="store_true")
    parser.add_argument("-s", "--setup", help="First time setup.", action="store_true")
    parser.add_argument("-b", "--backup", help="Run backup.", action="store_true")
    parser.add_argument("-r", "--restore", help="Run restore.", action="store_true")

    args = parser.parse_args(sys.argv[1:])

    try:
        if args.current:
            conf = Config.current_config()
            if conf is None:
                print("Not config found. You have to run setup.")
            else:
                bucket = conf["S3Bucket"]
                prefix = conf["S3Prefix"]
                root = conf["Root"]
                dirs = conf["Dirs"]
                print("The root folder from where backups are done is: {}".format(root))
                print("Folders to back up are: [{}]".format(" , ".join(dirs)))
                print("S3 location is: {}/{}".format(bucket, prefix))
                print("\n\n")
            sys.exit(0)
        if args.setup:
            Config.interactive_first_run()
            sys.exit(0)
        if args.backup:
            conf = Config().config
            log = Log(conf)
            decider = UploadDecider(log, conf)
            decider.run()
            sys.exit(0)
        if args.restore:
            conf = Config().config
            log = Log(conf)
            decider = DownloadDecider(log, conf)
            decider.run()
            sys.exit(0)
        parser.print_help()
    except ValueError as e:
        print("[ERROR] {}".format(e))
